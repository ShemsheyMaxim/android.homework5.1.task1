package com.maxim.listview.model;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by Максим on 15.05.2017.
 */

public class Contact implements Serializable {

    private String name;
    private String email;
    private String address;
    private String telephone;
    private int icon;


    public Contact(String name, String email, String address, String telephone, int icon) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.telephone = telephone;
        this.icon = icon;
    }


    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getTelephone() {
        return telephone;
    }

    public int getIcon() {
        return icon;
    }
}
