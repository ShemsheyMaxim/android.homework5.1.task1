package com.maxim.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.maxim.listview.model.Contact;

public class Main2Activity extends AppCompatActivity {
    TextView name;
    TextView email;
    TextView address;
    TextView telephone;
    ImageView icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Contact contact = (Contact) getIntent().getSerializableExtra("contact");

        name = (TextView) findViewById(R.id.textView_name_icon);
        email = (TextView) findViewById(R.id.textView_email_icon);
        address = (TextView) findViewById(R.id.textView_address_icon);
        telephone = (TextView) findViewById(R.id.textView_telephone_icon);
        icon = (ImageView) findViewById(R.id.imageView_icon);

        name.setText(contact.getName());
        email.setText(contact.getEmail());
        address.setText(contact.getAddress());
        telephone.setText(contact.getTelephone());
        icon.setImageResource(contact.getIcon());

    }
}
