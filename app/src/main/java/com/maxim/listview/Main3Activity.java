package com.maxim.listview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.maxim.listview.model.Contact;

public class Main3Activity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        final ImageView imageView = (ImageView) findViewById(R.id.imageView_new_contact);
        imageView.setImageResource(R.drawable.image_file_96);

        final EditText nameNewContact = (EditText) findViewById(R.id.textView_name_new_contact);
        final EditText emailNewContact = (EditText) findViewById(R.id.textView_email_new_contact);
        final EditText addressNewContact = (EditText) findViewById(R.id.textView_address_new_contact);
        final EditText telephoneNewContact = (EditText) findViewById(R.id.textView_telephone_new_contact);

        Button create = (Button) findViewById(R.id.button_create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameContact = nameNewContact.getText().toString();
                String emailContact = emailNewContact.getText().toString();
                String addressContact = addressNewContact.getText().toString();
                String telephoneContact = telephoneNewContact.getText().toString();
//                int imageViewNewContact = imageView.getImageAlpha();

                Intent intent = new Intent();
                intent.putExtra("nameContact", nameContact);
                intent.putExtra("emailContact", emailContact);
                intent.putExtra("addressContact", addressContact);
                intent.putExtra("telephoneContact", telephoneContact);
//                intent.putExtra("imageViewNewContact",R.drawable.image_file_96);
//                intent.putExtra("imageViewNewContact",imageViewNewContact);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
