package com.maxim.listview;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.maxim.listview.model.Contact;

import java.util.List;

/**
 * Created by Максим on 15.05.2017.
 */

public class ContactAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    List<Contact> contacts;

    ContactAdapter(Context context, List<Contact> contact) {
        contacts = contact;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Object getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = layoutInflater.inflate(R.layout.item_view, parent, false);
        }

        Contact c = getContact(position);

        ((TextView) view.findViewById(R.id.textView_name)).setText(c.getName());
        ((TextView) view.findViewById(R.id.textView_email)).setText(c.getEmail());
        ((ImageView) view.findViewById(R.id.imageView)).setImageResource(c.getIcon());
        return view;
    }

    private Contact getContact(int position) {
        return ((Contact) getItem(position));
    }
}
